package md.utm.lab.warehouse.service.impl;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import md.utm.lab.warehouse.mapper.StudentMapper;
import md.utm.lab.warehouse.model.Student;
import md.utm.lab.warehouse.service.StudentService;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Log4j2
@AllArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final RedisTemplate<String, String> redisTemplate;

    private final StudentMapper studentMapper;

    @Override
    public void insertNewStudent(List<Student> students) {

        for (Student student: students) {

            String studentKey = "student_id_" + student.getFirstName() + "_"+ student.getLastName();
            final boolean hasKey = redisTemplate.hasKey(studentKey);

            if (!hasKey){
                studentMapper.insertNewStudent(student);
                redisTemplate.opsForValue().set(studentKey, "");
                log.info("inset new student with id " + student.getId() + " with name " + student.getFirstName() +
                        " and last name  " + student.getLastName());
            } else {
                studentMapper.updateStudent(students);
                log.info("students updated");
            }

        }

    }

    @Override
    @Cacheable(value = "student", key = "#id")
    public Student getStudent(Long id) {
        log.info("student from db");
        return studentMapper.getStudent(id);
    }

    @Override
    public List<Student> allStudents() {
        return studentMapper.allStudents();
    }

    @Override
    public void deleteStudent(Long id) {
        studentMapper.deleteStudent(id);
    }

}
