package md.utm.lab.warehouse.service;

import md.utm.lab.warehouse.model.Student;

import java.util.List;

public interface StudentService {

    void insertNewStudent(List<Student> student);

    Student getStudent(Long id);

    List<Student> allStudents();

    void deleteStudent(Long id);

}
