package md.utm.lab.warehouse.mapper;

import md.utm.lab.warehouse.model.Student;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface StudentMapper {

    void insertNewStudent(Student student);

    void updateStudent(List<Student> student);

    Student getStudent(Long id);

    List<Student> allStudents();

    void deleteStudent(Long id);

}
