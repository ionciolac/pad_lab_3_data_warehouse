package md.utm.lab.warehouse.controller;

import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import md.utm.lab.warehouse.model.Student;
import md.utm.lab.warehouse.service.StudentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("student")
@Api(value = "student")
@AllArgsConstructor
public class StudentController {

    private final StudentService studentService;

    @PostMapping()
    public ResponseEntity insertStudents(@RequestBody List<Student> students){

        studentService.insertNewStudent(students);
        return new ResponseEntity(HttpStatus.CREATED);

    }

    @GetMapping()
    public ResponseEntity getAllStudents(){

        List<Student> students = studentService.allStudents();

        if (students.isEmpty())
            return new ResponseEntity(HttpStatus.NOT_FOUND);

        return new ResponseEntity(students, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity studentById(@PathVariable Long id){

        Student student = studentService.getStudent(id);

        if (student == null)
            return new ResponseEntity(HttpStatus.NOT_FOUND);

        return new ResponseEntity(student, HttpStatus.OK);
    }

    @DeleteMapping("/id")
    public ResponseEntity deleteStudentById(@PathVariable Long id){
        studentService.deleteStudent(id);
        return new ResponseEntity(HttpStatus.OK);
    }



}
